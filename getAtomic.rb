require 'bundler/inline'

gemfile do
	source "https://rubygems.org"
	gemspec
end

require 'atomic_red_team'

ARGV.each do|a|
	AtomicRedTeam.new.atomic_tests_for_technique(a).each do |atomic_test_yaml|
	  puts "#{atomic_test_yaml}"
	end
end
