<?php
$jsonselected = [];
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $json = json_decode(file_get_contents($_FILES["jsonfile"]["tmp_name"]), true);
  foreach ($json["techniques"] as $key => $value) {
    array_push($jsonselected, $value["techniqueID"]);
  }
}
$mitrejson = json_decode(file_get_contents("../atomic_red_team/enterprise-attack.json"), true);
$objects = $mitrejson["objects"];
function getByKey($skey, $objects){
  $return = [];
  foreach ($objects as $key => $value) {
    if ($value["type"] == $skey) {
      array_push($return, $value);
    }
  }
  return $return;
}

function getByTactic($skey, $objects){
  $return = [];
  foreach ($objects as $key => $value) {
    if ($value["type"] == "attack-pattern") {
      if ($value["kill_chain_phases"][0]["phase_name"] == str_replace(" ","-",strtolower($skey))) {
        array_push($return, $value);
      }
    }
  }
  return $return;
}

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <meta charset="utf-8">
    <title>Atomic Web Interface</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <form action="" method="post" enctype="multipart/form-data">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Upload a JSON file</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <input type="file" name="jsonfile" value="Upload a json file">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <input class="btn btn-primary" type="submit" name="" value="Upload JSON">
      </div>
    </div>
  </div>
</form>
</div>
    <div class="menu">
      <p class="button" data-toggle="modal" data-target="#upload">JSON <i class="fas fa-file-upload"></i></p>
      <p class="button" onclick="Start()">Next <i class="fas fa-chevron-right"></i></p>
    </div>
    <table class="table">

      <thead>
      <?php
        $tactics = getByKey("x-mitre-tactic", $objects);
        foreach ($tactics as $key => $value) {
            $patterns = getByTactic($value["name"], $objects);
          ?>
            <th scope="col"><?php echo $value["name"]." (".count($patterns).")"; ?></th>
          <?php
        }
      ?>
    </thead>
      <?php
      foreach ($tactics as $key => $value) {
        ?><tr><?php
          $patterns = getByTactic($value["name"], $objects);
          foreach ($patterns as $patternkey => $pattern) {
            ?>
              <td <?php if (in_array($pattern["external_references"][0]["external_id"], $jsonselected)){echo "class='active'";} ?> id="<?php echo $pattern["external_references"][0]["external_id"]; ?>"><?php echo $pattern["name"]; ?></td>
            <?php
          }
          ?></tr><?php
      }
      ?>
    </table>
    <form id="atomic" action="atomic.php" method="post">
      <input id="atomics" type="hidden" name="atomics" value="">
    </form>
    <script src="https://code.jquery.com/jquery-3.4.0.js"></script>
    <script src="https://unpkg.com/floatthead"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script>
    $("td").on("click", function(){
      if ($(this).attr('class') == "active") {
        $(this).removeClass('active');
      } else {
        $(this).addClass('active');
      }
    });
    var Start = function(){
      var active = $(".active");
      var ids = [];
      active.each(function(index) {
        ids.push($(this).attr("id"));
      });
      $("#atomics").val(ids);
      $("#atomic").submit();
    }
    var biggestSub = function(o){
      var biggest = 0;
      for (var i = 0; i < o.length; i++) {
        if (o[i].length > biggest) {
          biggest = o[i].length;
        }
      }
      return biggest;
    }
    $(()=>{
      var obj = [];
      $("tr").each(function(index) {
        obj[index] = $(this).children("td");
      });
      $("tr").each(function(index) {
        if (index != 0) {
          $(this)[0].innerHTML = "";
        }
      });
      for (var i = 0; i < biggestSub(obj); i++) {
        var row = $("<tr></tr>");
        $("th").each(function(index){
          if (obj[index+1][i]) {
            row[0].append(obj[index+1][i]);
          } else {
            var empty = $("<td></td>").text("");
            row[0].append(empty[0]);
          }
        });
        $("table").append(row);
      }
        $('table').floatThead();
    });
    </script>
  </body>
</html>
