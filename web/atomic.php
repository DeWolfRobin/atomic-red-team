<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <style media="screen">
      body {
        padding-left: 10px;
      }
    </style>
  </head>
  <body>
    <h1>Selected atomic tests:</h1>
    <?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST["atomics"])) {
          $atomics = explode(",",$_POST["atomics"]);
          foreach ($atomics as $key => $id) {
            $atomic = shell_exec("cd .. && ruby getAtomic.rb ".$id." && cd web");
            $atomic = explode("\n",trim(str_replace('} ',"},",str_replace('\#',"",str_replace('\n',"",str_replace("=>",": ",$atomic))))));
            foreach ($atomic as $key => $value) {
              $atomic[$key] = json_decode($value, true);
              $atomic[$key]["id"] = $id;
            }
            foreach ($atomic as $key => $value) {
              if ($value["name"]) {
              ?>
              <div class="form-group" id="<?php echo $value["id"];?>" name="<?php echo $value["name"]; ?>">
                <h2><?php echo $value["id"].": ".$value["name"] ?></h2>
                <?php if ($value["input_arguments"]) {foreach ($value["input_arguments"] as $key => $value) {
                  ?>
                    <label for=""><?php echo $value["description"]; ?></label>
                    <input class="form-control" type="text" name="<?php echo $key; ?>" value="<?php echo $value["default"]; ?>">
                  <?php
                }} ?>
              </div>
              <?php
              }
            }
          }
        }
    }

    ?>
<div class="menu">

  <p class="button" onclick="getCommands()">Start Tests <i class="fas fa-chevron-right"></i></p>
</div>
  <form action="exec.php" method="post">
    <input type="hidden" name="commands" id="commands" value="">
  </form>
  <script src="https://code.jquery.com/jquery-3.4.0.js"></script>
  <script src="https://unpkg.com/floatthead"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script>
  var getCommands = function(){
    var divs = $("div");
    var commands = [];
    divs.each(function(){
      if ($(this).attr("id") && $(this).attr("name")) {
        var tempcommand = "../execution-frameworks/ruby/go-atomic.rb -t "+$(this).attr("id")+" -n '"+$(this).attr("name")+"'";
        $(this).children("input").each(function(){
          tempcommand += " --input-"+$(this).attr("name")+"='"+$(this).val()+"'";
        });
        commands.push(tempcommand);
      }
    });
    commands = commands.join("|");
    $("#commands").val(commands);
    $("form").submit();
  }
  </script>
  </body>
</html>
